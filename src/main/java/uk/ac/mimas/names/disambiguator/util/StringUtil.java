package uk.ac.mimas.names.disambiguator.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



import org.apache.log4j.Logger;

/**
 * A number of string utility function used by the Disambiguator
 * 
 * @author danielneedham
 *
 */
public class StringUtil {
	private static final Logger logger = Logger.getLogger(StringUtil.class.getName());
	public static String corporateBodiesRegex = "((City|,)?( )?(University|College|School|Institute|Observatory|Royal|Academy)( (of|at|in|for the)?)?( (Wales)?)?)+";
	public static String normalisedCorporateBodiesRegex = "((University?)?((New |Royal |((Sixth|6th) Form ))?( ?College?|Academy?|Institute|School)( of)?)?)";
	
	/**
	 * Split element definition
	 * Contains an element and the delimiter by which it was split.
	 * 
	 * @author danielneedham
	 *
	 */
	public static class SplitElement{
		public String element;
		public boolean delimiter;
		
		public SplitElement(String v, boolean d){
			this.element = v;
			this.delimiter = d;
		}
	}
	
	/**
	 * MD5 Hash of an input string
	 * @param sIn the string to hash
	 * @return The hashed string.
	 */
	public static String md5String(String sIn){
		String returnString = "";
		byte[] defaultBytes = sIn.getBytes();
		try{
			MessageDigest algorithm = MessageDigest.getInstance("MD5");
			algorithm.reset();
			algorithm.update(defaultBytes);
			byte messageDigest[] = algorithm.digest();
			StringBuffer hexString = new StringBuffer();
			for(int i =0; i < messageDigest.length; i++)
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			returnString = hexString.toString();
		}
		catch(Exception e){
			logger.error("Error hashing " + sIn + " - " + e.toString());
		}
		return returnString;
	}
	
	/**
	 * Returns an exception stack trace as a string, generally for output into a log file.
	 * @param exception The generated exception
	 * @return The stack trace as a string.
	 */
	public static String getStackTraceAsString(Exception exception)
	{
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		
		pw.println(exception.getClass().getName());
		
		pw.println(exception.getMessage());
		exception.printStackTrace(pw);
		return sw.toString();
	}
	
	/**
	 * Gets only the chracters within a string.
	 * @param s The string to process
	 * @return The characters from the input string, lower cased.
	 */
	public static String getCharactersOnly(String s){
		String normalisedString = s;
		try{
		normalisedString =normalisedString.toLowerCase();
		normalisedString =normalisedString.replaceAll("[^\\w]", "");
		normalisedString =normalisedString.trim();
		}
		catch(Exception e){
			logger.error("Error getting characters from " + s + " - " + e.toString());
		}
		return normalisedString;
	}
	
	/**
	 * Splits an input string on the specified delimiter, which could be a regular expression pattern
	 * @param text The string to split
	 * @param patternText The delimiter
	 * @return A list of split elements
	 */
	public static ArrayList<SplitElement>  splitWithDelimiter(String text, String patternText){
		
		 ArrayList<SplitElement> results = new ArrayList<SplitElement>(0);
		try{
			
		
			 Pattern pattern = Pattern.compile(patternText,Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);
		     Matcher matcher = pattern.matcher(text);
		     matcher.useAnchoringBounds(true);
		     int lastMatch = 0;
		     String delimiter = "";
		     String value = "";
		     SplitElement tempElement;
		     while (matcher.find()){
		    	 delimiter = matcher.group().trim();
		    	 if(matcher.start() != lastMatch){
		    		 value = text.substring(lastMatch,matcher.start()).trim();
		    		 tempElement = new SplitElement(value, false);
		    		 results.add(tempElement);
		    	 }
		    	 
		    	 
		    	 results.add(new SplitElement(delimiter, true));
		         lastMatch = matcher.end();
		         break;
		     }
		     if(lastMatch <= text.length()-1){
		    	 	value = text.substring(lastMatch).trim();
		    		results.add(new SplitElement(value, false));
		     }
		     
	
		     		
			}
			catch(Exception e){
				logger.error("Error trying to split " + text + " with pattern " +patternText+ "- " + e.toString());
				e.printStackTrace();
			}
			return results;
	}
	

	
	
}
