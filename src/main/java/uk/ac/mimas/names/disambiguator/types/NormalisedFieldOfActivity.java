package uk.ac.mimas.names.disambiguator.types;


/**
 * 
 * Normalised Field of Activity
 * 
 * @author danielneedham
 *
 */
public class NormalisedFieldOfActivity  extends NormalisedAttribute{
	public String fieldOfActivity;
	
	/**
	 *  Constructor 
	 * 
	 */
	public NormalisedFieldOfActivity(){
		super();
		this.fieldOfActivity = "";
	}
	
	/**
	 * Constructor
	 * 
	 * @param foa The field of activity
	 */
	public NormalisedFieldOfActivity(String foa){
		super();
		this.fieldOfActivity = foa;
	}

	/**
	 * Gets the field of activity
	 * @return The field of activity
	 */
	public String getFieldOfActivity() {
		return fieldOfActivity;
	}

	/**
	 * Sets the field of activity string
	 * 
	 * @param foa The field of activity;
	 */
	public void setFieldOfActivity(String fieldOfActivity) {
		this.fieldOfActivity = fieldOfActivity;
	}
	
	
}
