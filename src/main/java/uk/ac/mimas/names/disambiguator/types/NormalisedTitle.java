package uk.ac.mimas.names.disambiguator.types;

/**
 * Normalised Title
 * 
 * @author danielneedham
 *
 */
public class NormalisedTitle  extends NormalisedAttribute{
	public String title; 									// A salutation
	
	/**
	 * Constructor
	 * 
	 */
	public NormalisedTitle(){
		super();
		title = "";
	}
	/**
	 * Get the title
	 * @return The title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * Set the title
	 * 
	 * @param title The title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
}
