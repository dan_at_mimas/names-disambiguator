package uk.ac.mimas.names.disambiguator.types;

/**
 * Normalised Identifier
 * 
 * @author danielneedham
 *
 */

public class NormalisedIdentifier extends NormalisedAttribute{
		public String identifier;
		public String basisFor;
		/**
		 * Constructor
		 */
		public NormalisedIdentifier() {
			super();
			
		}
		
		/**
		 * Get the identifier string associated with this identifier
		 * @return Identifier string
		 */
		public String getIdentifier() {
			return identifier;
		}
		/**
		 * Set the identifier string associated with this identifier
		 * @param identifier
		 */
		public void setIdentifier(String identifier) {
			this.identifier = identifier;
		}
		/**
		 * Get the basis for this identifier
		 * @return Basis for the identifier
		 */
		public String getBasisFor() {
			return basisFor;
		}
		/**
		 * Set the basis for this identifier
		 * @param basisFor The basis for this identifier.
		 */
		public void setBasisFor(String basisFor) {
			this.basisFor = basisFor;
		}
		
		
		
}
