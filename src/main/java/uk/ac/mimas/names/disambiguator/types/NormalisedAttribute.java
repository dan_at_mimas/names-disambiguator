package uk.ac.mimas.names.disambiguator.types;

/**
 * 
 * All Normalised attributes inherit from this base class, which provides 
 * attributes to track source and match scores
 * 
 * @author Dan Needham <daniel.needham@manchester.ac.uk>
 * 
 */



public class NormalisedAttribute{
	
	
	public String sourceID;						// An identifier used by the source to identify the source record
	public String sourceName;					// The name of the source
	public String sourceURL;					// The URL of the source
	public double matchScore;					// A match score associated with this attribute and the concept of the individual it is assigned to.
	
	
	/**
	 * Constructor
	 * 
	 * @param sourceID An identifier used by the source to identify the record
	 * @param sourceName The name of the source
	 * @param sourceURL	The URL of the source
	 * @param matchScore
	 */
	public NormalisedAttribute(String sourceID, String sourceName,
			String sourceURL, double matchScore) {
		super();
		this.sourceID = sourceID;
		this.sourceName = sourceName;
		this.sourceURL = sourceURL;
		this.matchScore = matchScore;
	}
	
	/**
	 * Constructor
	 * 
	 */
	public NormalisedAttribute(){
		super();
		this.sourceID = "";
		this.sourceName = "";
		this.sourceURL = "";
		this.matchScore = 0.0;
	}
	
	/**
	 * Gets the source id
	 * @return The identifier of the source record that this attribute came from
	 */
	public String getSourceID() {
		return sourceID;
	}
	
	/**
	 * Set source id
	 * 
	 * @param sourceID the identifier of the source record that this attribute came from
	 */
	public void setSourceID(String sourceID) {
		this.sourceID = sourceID;
	}
	
	/**
	 * Gets the name of the source that provided this attribute
	 * 
	 * @return The source name for this attribute 
	 */
	public String getSourceName() {
		return sourceName;
	}
	
	/**
	 * Set the name of the source that provided this attribute
	 * 
	 * @param sourceName The source name for this attribute
	 */
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	
	/**
	 * Gets the URL of the source that provided this attribute
	 * 
	 * @return The URL of the source that provided this attribute
	 */
	public String getSourceURL() {
		return sourceURL;
	}
	
	/**
	 * Sets the URL of the source that provided this attribute
	 * 
	 * @param sourceURL The URL of the source that provided this attribute
	 */
	public void setSourceURL(String sourceURL) {
		this.sourceURL = sourceURL;
	}
	
	/**
	 * Gets the score that was derived when the source record that this attribute
	 * came from was compared with the record it was merged into.
	 * 
	 * @return The match score of the overall comparison between the source record and the record this attribute was merged into
	 */
	public double getMatchScore() {
		return matchScore;
	}
	
	/**
	 * 
	 * Sets the match score that was deribed when the source record that this attribute came from was 
	 * compared with the record it was merged into.
	 * 
	 * @param matchScore The score derived from the comparison between the source record and the eventual names record.
	 */
	public void setMatchScore(double matchScore) {
		this.matchScore = matchScore;
	}
	
}
