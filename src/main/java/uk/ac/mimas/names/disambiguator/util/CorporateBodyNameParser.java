package uk.ac.mimas.names.disambiguator.util;

import java.util.ArrayList;
import uk.ac.mimas.names.disambiguator.types.NormalisedNameComponent;
import uk.ac.mimas.names.disambiguator.util.StringUtil;
import uk.ac.mimas.names.disambiguator.util.StringUtil.SplitElement;
public class CorporateBodyNameParser {

	
	ArrayList<NormalisedNameComponent> nameComponents;
	public String normalisedName = "";
	public int namesStart = 0;
	public boolean containsUnknown;

	
	public class NameElement{
		public int position;
		public String chars;
		public int type;
	
	}
	
	public CorporateBodyNameParser(){
		nameComponents = new ArrayList<NormalisedNameComponent>();
	}
	
	
	
	public void parse(String nameString){
		// split the incoming string using the predefined institution regex
		ArrayList<SplitElement> splitElements = StringUtil.splitWithDelimiter(nameString.trim(), StringUtil.corporateBodiesRegex);
		NormalisedNameComponent tempComponent;
		int rankOrder = 0;
		for(SplitElement element : splitElements){
			// if this element is a delimiter defined in the regex then add it to the list
			if(element.delimiter == true){
				tempComponent = new NormalisedNameComponent();
				 tempComponent.setChars(element.element.trim());
				 this.nameComponents.add(tempComponent);
				 rankOrder++;
			}
			else{
				// it must be part of the institution name text, which 
				String[] elementSplit = element.element.replaceAll(",", "").trim().split(" ");
				for(String splitElement : elementSplit){
					 tempComponent = new NormalisedNameComponent();
					 tempComponent.setChars(splitElement);
					 this.nameComponents.add(tempComponent);
					 rankOrder++;
				}
			}
		}
		
		
		
		
	}
	
	public ArrayList<NormalisedNameComponent> getNameComponents(){
		return this.nameComponents;
	}
	
	

	


	
	public void clear(){
		
		nameComponents = new ArrayList<NormalisedNameComponent>();
	
		namesStart = 0;
		
	}

}
