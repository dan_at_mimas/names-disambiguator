package uk.ac.mimas.names.disambiguator.types;
import java.util.ArrayList;
import uk.ac.mimas.names.disambiguator.util.StringUtil;
import java.util.Date;
/**
 * 
 * Normalised Name
 * 
 * @author danielneedham
 *
 */

public class NormalisedName extends NormalisedAttribute{
		public String name;											// The original name string from the source.
		public String normalisedName;								// The name string after any normalisation has occurred.
		public ArrayList<NormalisedNameComponent> familyNames;		// A list of family name components in the name string
		public ArrayList<NormalisedNameComponent> givenNames;		// A list of given name components in the name string.
		public Date usedFrom;										// Name in use from
		public Date usedUntil;										// Name in use until
		public static long minimumDate = 0;
		public static long maximumDate = 253402214400000L;
		/**
		 *  Constructor
		 * 
		 */
		public NormalisedName(){
			super();
			name = "";
			normalisedName = "";
			familyNames = new ArrayList<NormalisedNameComponent>();
			givenNames = new ArrayList<NormalisedNameComponent>();
			usedFrom = new Date(minimumDate);
			usedUntil = new Date(maximumDate);
		
		}
		
		/**
		 * Constructor
		 *
		 * 
		 * @param chars The original name string in the source record
		 */
		public NormalisedName(String chars){
			super();
			name = chars;
			normalisedName = "";
			familyNames = new ArrayList<NormalisedNameComponent>();
			givenNames = new ArrayList<NormalisedNameComponent>();
			usedFrom = new Date(minimumDate);
			usedUntil = new Date(maximumDate);
		}
		
		/**
		 * Get the original name string
		 * 
		 * @return The original name 
		 */

		public String getName() {
			return name;
		}
		
		/**
		 * Set the original name
		 * 
		 * @param name The original name in the source record
		 */

		public void setName(String name) {
			this.name = name;
		}
		
		/**
		 * Get the normalised form of the name
		 * 
		 * @return The normalised name
		 */
		
		public String getNormalisedName() {
			return normalisedName;
		}
		
		/**
		 * Set the name form after any normalisation has occurred
		 * 
		 * @param normalisedName The name string after normalisation has been applied
		 */

		public void setNormalisedName(String normalisedName) {
			this.normalisedName = normalisedName;
		}
		
		/**
		 * Get the family name components of the name
		 * 
		 * @return The family name components of the name. Empty if there aren't any.
		 */
		public ArrayList<NormalisedNameComponent> getFamilyNames(){
			return this.familyNames;
		}
		
		/**
		 * Add a family name component
		 * 
		 * @param n The family name component to add
		 */
		public void addFamilyName(NormalisedNameComponent n){
			this.familyNames.add(n);
		}
		
		/**
		 * Get the given name components of the name
		 * 
		 * @return The given name components of the name. Empty if there aren't any.
		 */
		
		public ArrayList<NormalisedNameComponent> getGivenNames(){
			return this.givenNames;
		}
		
		/**
		 * Add a given name component
		 * 
		 * @param n The given name component to add
		 */
		
		public void addGivenName(NormalisedNameComponent n){
			this.givenNames.add(n);
		}
		
		/**
		 * Get the date the name was used from
		 * @return Date used from
		 */
		
		public Date getUsedFrom(){
			return this.usedFrom;
		}
		
		/**
		 * Set the date the name was used from
		 * @param f The date the name was used from
		 */
		public void setUsedFrom(Date f){
			this.usedFrom = f;
		}
		
		
		
		
		/**
		 * Get the date the name was used until
		 * @return The date the name was used until
		 */
		public Date getUsedUntil(){
			return this.usedUntil;
		}
		/**
		 * Set the date the name was used until
		 *
		 * @param u the date the name was used until
		 */
		public void setUsedUntil(Date u){
			this.usedUntil = u;
		}
		
		
		
		
		/**
		 * States whether a date has been set.
		 * 
		 * For this purpose a date at the beginning of Epoch time or a date at the maximum value of long
		 * is considered not to be set
		 * @return
		 */
		public boolean dateSet(Date d){
			return d.getTime() == minimumDate || d.getTime() == maximumDate ? false : true;
		}
		
		
		/**
		 *  Compares the given name collections and returns a percentage matchedness.
		 * 
		 * @param n A list of normalised name components
		 * @param c Another list of normalised name components to compare against
		 * @return A score for how well they matched
		 */
		 private double compareCollections(ArrayList<NormalisedNameComponent>n ,ArrayList<NormalisedNameComponent> c){
				float r = 0;
				ArrayList<NormalisedNameComponent> smallest;
				int i;
				i = 0;

				// Determine which is the smallest collection
				if(n.size() > c.size()){
					smallest =c;	
				}
				else{
					smallest = n;
				}

				/*
				 * Iterate through the largest collection's components attempting to match
				 * against the same position in the smallest collection.
				 * 
				 * At the first match fail, exit.
				 * 
				 */
		
				for(i =0;i < n.size();i++){
					String nChars = StringUtil.getCharactersOnly(n.get(i).getChars());
					if(c.size() > i){
						String cChars = StringUtil.getCharactersOnly(c.get(i).getChars());
						if(!nChars.equalsIgnoreCase(cChars)){
							// FIXME This is just a work around that allows comparison between full names
							// and initials
							if(nChars.length() == 1 || cChars.length() == 1){
								if(!(nChars.toLowerCase().charAt(0) == cChars.toLowerCase().charAt(0))){
									break;
								}
							}
							else{
								break;
							}
						}
					}
					else 
						break;
				
				}
			
				// if the number of matches is equal to the smallest 
				// length collection then we have a potential match
				// eg J.B & J. would match, but J.B & J.F would not
				// basically discard any results in which the smallest of the
				// two collections don't fully match the corresponding characters
				// in the largest collection. (This might need altering.)
				if(i == smallest.size()){
					
					if(n.equals(smallest)){
						r = (float) (100 / c.size()) * i;
					}
					else{
						r = (float) (100 / n.size()) * i;
					}
				}
					
			
			return r;
		 }
		
	 /**
	  *  Compare this normalised name against another one and derive a score of their matchedness
	  * 
	  * @param compareName The name to compare with
	  * @return An array containing a score (0-100) for how well the family names match and for how well the given names match
	  */
		
		public double[] compare(NormalisedName compareName){
			double score[] = new double[2];
			score[0] = score[1] =  0.0;
			if(this.getFamilyNames().size() != 0
					&& compareName.getFamilyNames().size() !=0){
				score[0] = this.compareCollections(this.getFamilyNames(), compareName.getFamilyNames()); 				// Compare the family names
			}
			
			if(this.getGivenNames().size() != 0
					&& compareName.getGivenNames().size() !=0){
				score[1] = this.compareCollections(this.getGivenNames(), compareName.getGivenNames());					// Compare the given names
			}
			return score;
		}
		
		
		
}
