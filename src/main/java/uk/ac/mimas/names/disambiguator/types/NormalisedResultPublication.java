package uk.ac.mimas.names.disambiguator.types;


/**
 * NormalisedResultPublication
 * 
 * @author danielneedham
 *
 */
public class NormalisedResultPublication  extends NormalisedAttribute{
	public String title;							
	public String number;							
	public String volume;							
	public String edition;
	public String series;
	public String issue;
	public String dateOfPublication;
	public String abs;										// abstract
	
	/**
	 * Constructor
	 * 
	 * @param title The title of the publication
	 * @param number The number of the journal in which the publication appears
	 * @param volume The volume of the journal in which the publication appears
	 * @param edition The edition of the journal in which the publication appears
	 * @param series The series of the journal in which the publication appears
	 * @param issue The issue of the journal in which the publication appeears
	 * @param dateOfPublication The date of publication
	 * @param abs The abstract of the publication
	 */
	public NormalisedResultPublication(String title, String number,
			String volume, String edition, String series, String issue,
			String dateOfPublication, String abs) {
		super();
		this.title = title;
		this.number = number;
		this.volume = volume;
		this.edition = edition;
		this.series = series;
		this.issue = issue;
		this.dateOfPublication = dateOfPublication;
		this.abs = abs;
	}
	
	/**
	 *  Constructor
	 * 
	 */
	public NormalisedResultPublication(){
		super();
		this.title = "";
		this.number = "";
		this.volume = "";
		this.edition = "";
		this.series = "";
		this.issue = "";
		this.dateOfPublication = "";
		this.abs = "";
	}
	
	/**
	 * Get the title of the result publication
	 * @return The title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * Set the title of the result publication
	 * @param title The title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * Get the number of the journal in which the publication appears. 
	 * @return The number of the journal, empty if not set.
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * Set the number of the journal in which the publication appears. 
	 * @param number The number of the journal
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * Get the volume of the journal in which the publication appears. 
	 * @return The volume of the journal, empty if not set.
	 */
	public String getVolume() {
		return volume;
	}
	/**
	 * Set the volume of the journal in which the publication appears. 
	 * @param volume The volume of the journal
	 */
	public void setVolume(String volume) {
		this.volume = volume;
	}
	/**
	 * Get the edition of the journal in which the publication appears. 
	 * @return The edition of the journal, empty if not set.
	 */
	public String getEdition() {
		return edition;
	}
	/**
	 * Set the edition of the journal in which the publication appears. 
	 * @param edition The edition of the journal
	 */
	public void setEdition(String edition) {
		this.edition = edition;
	}
	/**
	 * Get the series of the journal in which the publication appears. 
	 * @return The series of the journal, empty if not set.
	 */
	public String getSeries() {
		return series;
	}
	/**
	 * Set the series of the journal in which the publication appears. 
	 * @param series The series of the journal
	 */
	public void setSeries(String series) {
		this.series = series;
	}
	/**
	 * Get the issue of the journal in which the publication appears. 
	 * @return The issue of the journal, empty if not set.
	 */
	public String getIssue() {
		return issue;
	}
	/**
	 * Set the issue of the journal in which the publication appears. 
	 * @param issue The issue of the journal
	 */
	public void setIssue(String issue) {
		this.issue = issue;
	}
	/**
	 * Get the date of publication . 
	 * @return The date of publication, empty if not set.
	 */
	public String getDateOfPublication() {
		return dateOfPublication;
	}
	/**
	 * Set date of publication. 
	 * @param dateOfPublication The date of publication
	 */
	public void setDateOfPublication(String dateOfPublication) {
		this.dateOfPublication = dateOfPublication;
	}
	/**
	 * Get the abstract. 
	 * @return The abstract.
	 */
	public String getAbs() {
		return abs;
	}
	
	/**
	 *  Set the abstract
	 * 
	 * @param abs The abstract
	 */
	public void setAbs(String abs) {
		this.abs = abs;
	}
	
	

}
