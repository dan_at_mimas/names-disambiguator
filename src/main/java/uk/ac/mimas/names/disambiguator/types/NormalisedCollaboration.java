package uk.ac.mimas.names.disambiguator.types;
import uk.ac.mimas.names.disambiguator.NamesDisambiguator;
/**
 * Normlised Collaboration
 * 
 * @author danielneedham
 *
 */
public class NormalisedCollaboration extends NormalisedAttribute{
			public String collaborationName;
			public int collaborationNamesID;
			public NormalisedRecord collaboration;

			/**
			 * Constructor
			 */
			public NormalisedCollaboration() {
				super();
				collaborationName = "";
				collaborationNamesID = -1;
			}

			/** 
			 * Class constructor.
			 */
			
			public NormalisedCollaboration(String name){
				super();
				this.collaborationName = name;
				this.collaborationNamesID = -1;
				collaboration = new NormalisedRecord();
				
			}
			
			/** 
			 * Class constructor.
			 */
			
			public NormalisedCollaboration(String name, int id){
				super();
				this.collaborationName = name;
				this.collaborationNamesID = id;
				collaboration = new NormalisedRecord();
				
			}

			/**
			 * Get the normalised record associated with this collaboration
			 * @return The normalised record associated with this collaboration
			 */
			public NormalisedRecord getCollaboration() {
				return collaboration;
			}

			/**
			 * Set the normalised record associated with this collaboration
			 * @param collaboration The normalised record to set as a collaboration
			 */
			public void setCollaboration(NormalisedRecord collaboration) {
				this.collaboration = collaboration;
			}


			/**
			 * @return the collaborationNamesID
			 */
			public int getCollaborationNamesID() {
				return collaborationNamesID;
			}

			/**
			 * @param collaborationNamesID the collaborationID to set
			 */
			public void setCollaborationNamesID(int collaborationNamesID) {
				this.collaborationNamesID = collaborationNamesID;
			}


			/**
			 * Gets the collaboration name
			 *
			 * @returns The name of the collaboration
			 */
			
			public String getCollaborationName() {
				return collaborationName;
			}
			
			/**
			 * Sets the collaboration name
			 *
			 * @param collaborationName The name of the collaboration
			 */
			
			public void setCollaborationName(String collaborationName) {
				this.collaborationName = collaborationName;
			}

			/**
			 * Compare this collaboration against another one and derive a match score
			 * 
			 *
			 * @param collaborationRecord The normalised collaboration to compare with
			 */
			
			public double match(NormalisedCollaboration comparisonRecord){
				
				NamesDisambiguator n = new NamesDisambiguator();

				return n.match(this.getCollaboration(), comparisonRecord.getCollaboration());
			}

			
		
}
