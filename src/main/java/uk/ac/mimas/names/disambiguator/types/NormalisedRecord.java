package uk.ac.mimas.names.disambiguator.types;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import uk.ac.mimas.names.disambiguator.util.StringUtil;
import uk.ac.mimas.names.disambiguator.NamesDisambiguator;
import java.util.Date;
/**
 * 
 * A normalised representation of a source record
 * 
 * @author Dan Needham <daniel.needham@manchester.ac.uk>
 * 
 */


public class NormalisedRecord{
	
	// Record entity types
	public static final short INDIVIDUAL = 1;
	public static final short INSTITUTION = 10;
	public static final short UNKNOWN = 0;
	
	
	public short type; 																// the type of this record
	

	// The attributes associated with this record
	public ArrayList<NormalisedName> normalisedNames;
	public ArrayList<NormalisedTitle> normalisedTitles;
	public ArrayList<NormalisedAffiliation> normalisedAffiliations;
	public ArrayList<NormalisedFieldOfActivity> normalisedFieldsOfActivity;
	public ArrayList<NormalisedResultPublication> normalisedResultPublications;
	
	public ArrayList<NormalisedName> normalisedEarlierNames;
	public ArrayList<NormalisedHomepage> normalisedHomepages;
	public ArrayList<NormalisedIdentifier> normalisedIdentifiers;
	public ArrayList<NormalisedCollaboration> normalisedCollaborations;
	
    public HashMap<NormalisedRecord, Double> matchResults;							// A list of matches score against other normalised records

    
    /**
     * Constructor
     * 
     * @param entity_type The entity type of this record
     */
	public NormalisedRecord(short entity_type){
		
		
		normalisedNames = new ArrayList<NormalisedName>();
		normalisedTitles = new ArrayList<NormalisedTitle>();
		normalisedAffiliations = new ArrayList<NormalisedAffiliation>();
		normalisedFieldsOfActivity = new ArrayList<NormalisedFieldOfActivity>();
		normalisedResultPublications = new ArrayList<NormalisedResultPublication>();
		normalisedEarlierNames = new ArrayList<NormalisedName>();
		normalisedHomepages = new ArrayList<NormalisedHomepage>();
		normalisedIdentifiers = new ArrayList<NormalisedIdentifier>();
		normalisedCollaborations = new ArrayList<NormalisedCollaboration>();
		matchResults = new HashMap<NormalisedRecord, Double>();
		type = entity_type;
		
	}
	
	/**
     * Constructor
     * 
     * 
     */
	public NormalisedRecord(){

		
		normalisedNames = new ArrayList<NormalisedName>();
		normalisedTitles = new ArrayList<NormalisedTitle>();
		normalisedAffiliations = new ArrayList<NormalisedAffiliation>();
		normalisedFieldsOfActivity = new ArrayList<NormalisedFieldOfActivity>();
		normalisedResultPublications = new ArrayList<NormalisedResultPublication>();
		normalisedEarlierNames = new ArrayList<NormalisedName>();
		normalisedHomepages = new ArrayList<NormalisedHomepage>();
		normalisedIdentifiers = new ArrayList<NormalisedIdentifier>();
		normalisedCollaborations = new ArrayList<NormalisedCollaboration>();
		matchResults = new HashMap<NormalisedRecord, Double>();
		type = UNKNOWN;
		
	}
	
	/**
	 * Get the normalised names associated with this entity
	 * 
	 * @return The list of normalised names associated with this entity
	 */
	public ArrayList<NormalisedName> getNormalisedNames() {
		return normalisedNames;
	}

	/**
	 * Uniquely add a normalised name. 
	 * 
	 * If the name already exists for this entity then the operation is aborted
	 * 
	 * @param name The normalised name to add
	 * @return Whether or not the operation was successful
	 */

	public boolean addNormalisedName(NormalisedName name) {
		
		for(NormalisedName existing : this.getNormalisedNames())
			if(existing.getName().equalsIgnoreCase(name.getName()))
				return false;

		this.getNormalisedNames().add(name);
		return true;
	}
	
	/**
	 * 
	 * Get the normalised titles for this entity
	 * 
	 * @return The normalised titles for this entity
	 */
	
	public ArrayList<NormalisedTitle> getNormalisedTitles() {
		
		return normalisedTitles;
	}

	/**
	 * Uniquely add a normalised title. 
	 * 
	 * If the title already exists for this entity then the operation is aborted
	 * 
	 * @param title The normalised title to add
	 * @return Whether or not the operation was successful
	 */

	public boolean addNormalisedTitle(NormalisedTitle title) {
		for(NormalisedTitle existing : this.getNormalisedTitles())
			if(existing.getTitle().equalsIgnoreCase(title.getTitle()))
				return false;
		this.getNormalisedTitles().add(title);
		return true;
	}
	
	/**
	 * 
	 * Get the list of match results associated with this record
	 * 
	 * @return The list of match results (NormalisedRecords and their match score when compared with this one)
	 */
	
	public HashMap<NormalisedRecord, Double> getMatchResults(){
		return this.matchResults;
	}
	
	
	
	public NormalisedRecord getBestMatch(){
		Map.Entry<NormalisedRecord, Double> maxEntry = null;

		for (Map.Entry<NormalisedRecord, Double> entry : this.getMatchResults().entrySet())
		{
		    if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
		    {
		        maxEntry = entry;
		    }
		}
		return maxEntry.getKey();
	}
	
	public ArrayList<NormalisedRecord> getAllMatching(double threshold){
		ArrayList<NormalisedRecord> validMatches = new ArrayList<NormalisedRecord>();

		for (Map.Entry<NormalisedRecord, Double> entry : this.getMatchResults().entrySet())
		{
		    if(entry.getValue() >= threshold){
		    	validMatches.add(entry.getKey());
		    }
		}
		return validMatches;
	}
	
	/**
	 * Add a match result (A normalised record and it's score when compared with this one)
	 * 
	 * @param record
	 * @param score
	 */
	public void addMatchResults(NormalisedRecord record, Double score){
		this.matchResults.put(record, score);
	}
	
	/**
	 * Uniquely add a normalised affiliation. 
	 * 
	 * If the affiliation already exists for this entity then the operation is aborted
	 * 
	 * @param affiliation The normalised affiliation to add
	 * @return Whether or not the operation was successful
	 */
	
	public boolean addNormalisedAffiliation(NormalisedAffiliation affiliation) {
		for(NormalisedAffiliation existing : this.getNormalisedAffiliations())
			if(existing.match(affiliation) >= 50.0)
				return false;
		this.getNormalisedAffiliations().add(affiliation);
		return true;
	}
	
	/**
	 * Get the normalised affiliations for this entity
	 * 
	 * @return The normalised affiliations for this entity
	 */
	public ArrayList<NormalisedAffiliation> getNormalisedAffiliations() {
		
		return normalisedAffiliations;
	}
	
	/**
	 * Uniquely add a normalised field of activity. 
	 * 
	 * If the field of activity already exists for this entity then the operation is aborted
	 * 
	 * @param foa The normalised field of activity to add
	 * @return Whether or not the operation was successful
	 */
	
	public boolean addNormalisedFieldOfActivity(NormalisedFieldOfActivity foa) {
		for(NormalisedFieldOfActivity existing : this.getNormalisedFieldsOfActivity())
			if(existing.getFieldOfActivity().equalsIgnoreCase(foa.getFieldOfActivity()))
				return false;
		this.getNormalisedFieldsOfActivity().add(foa);
		return true;
	}
	
	/**
	 * Get the fields of activity associated with this entity
	 * 
	 * @return The fields of activity associated with this entity
	 */
	public ArrayList<NormalisedFieldOfActivity> getNormalisedFieldsOfActivity() {
		return normalisedFieldsOfActivity;
	}
	
	
	/**
	 * Uniquely add a normalised result publication. 
	 * 
	 * If the result publication title already exists for this entity then the operation is aborted
	 * 
	 * @param rp The normalised result publication to add
	 * @return Whether or not the operation was successful
	 */
	
	public boolean addNormalisedResultPublication(NormalisedResultPublication rp) {
		for(NormalisedResultPublication existing : this.getNormalisedResultPublications())
			if(StringUtil.getCharactersOnly(existing.getTitle()).equalsIgnoreCase(StringUtil.getCharactersOnly(rp.getTitle())))
				return false;
		this.getNormalisedResultPublications().add(rp);
		return true;
	}
	
	/**
	 * Gets the result publications associated with this entity
	 * 
	 * @return The result publications associated with this entity
	 */
	public ArrayList<NormalisedResultPublication> getNormalisedResultPublications() {
		return normalisedResultPublications;
	}
	
	
	/**
	 *  Get the earlier names associated with this entity
	 * @return
	 */
	
	public ArrayList<NormalisedName> getNormalisedEarlierNames() {
		return normalisedEarlierNames;
	}
	
	/**
	 * Uniquely add a normalised earlier name. 
	 * 
	 * If the name already exists for this entity then the operation is aborted
	 * 
	 * @param name The normalised earlier name to add
	 * @return Whether or not the operation was successful
	 */

	public boolean addNormalisedEarlierName(NormalisedName name) {
		
		for(NormalisedName existing : this.getNormalisedEarlierNames())
			if(existing.getName().equalsIgnoreCase(name.getName())
				&& existing.getUsedFrom().getTime() == name.getUsedFrom().getTime()
				&& existing.getUsedUntil().getTime() == name.getUsedUntil().getTime())
				return false;

		this.getNormalisedEarlierNames().add(name);
		return true;
	}
	
	
	
	
	/**
	 * Get the homepages associated with this entity
	 * @return
	 */

	public ArrayList<NormalisedHomepage> getNormalisedHomepages() {
		return normalisedHomepages;
	}
	
	
	/**
	 * Uniquely add a normalised homepage. 
	 * 
	 * If the homepage already exists for this entity then the operation is aborted
	 * 
	 * @param foa The normalised homepage to add
	 * @return Whether or not the operation was successful
	 */
	
	public boolean addNormalisedHomepage(NormalisedHomepage homepage) {
		for(NormalisedHomepage existing : this.getNormalisedHomepages())
			if(existing.getURL().equalsIgnoreCase(homepage.getSourceURL()))
				return false;
		this.getNormalisedHomepages().add(homepage);
		return true;
	}
	
	

	/**
	 * Get the identifiers associated with this entity
	 * @return
	 */
	public ArrayList<NormalisedIdentifier> getNormalisedIdentifiers() {
		return normalisedIdentifiers;
	}
	
	/**
	 * Uniquely add a normalised identifier. 
	 * 
	 * If the identifier with same basis for already exists for this entity then the operation is aborted
	 * 
	 * @param foa The normalised identifier to add
	 * @return Whether or not the operation was successful
	 */
	
	public boolean addNormalisedIdentifier(NormalisedIdentifier identifier) {
		for(NormalisedIdentifier existing : this.getNormalisedIdentifiers())
			if(existing.getIdentifier().equalsIgnoreCase(identifier.getIdentifier())
				&& existing.getBasisFor().equalsIgnoreCase(identifier.getBasisFor()))
				return false;
		this.getNormalisedIdentifiers().add(identifier);
		return true;
	}
	

	/**
	 * Get the collaborative relationships associated with this entity
	 * @return
	 */
	public ArrayList<NormalisedCollaboration> getNormalisedCollaborations() {
		return normalisedCollaborations;
	}
	
	/**
	 * Uniquely add a normalised collaboration. 
	 * 
	 * If the collaborative relationship already exists for this entity then the operation is aborted
	 * 
	 * @param foa The normalised collaboration to add
	 * @return Whether or not the operation was successful
	 */
	
	public boolean addNormalisedCollaboration(NormalisedCollaboration collaboration) {
		NamesDisambiguator n = new NamesDisambiguator();
		for(NormalisedCollaboration existing : this.getNormalisedCollaborations())
		{
			if(n.match(existing.getCollaboration(), collaboration.getCollaboration()) >= 75.00)
				return false;
		}
		this.getNormalisedCollaborations().add(collaboration);
		return true;
	}
	

	/**
	 * Gets the entity type
	 * 
	 * @return The entity type
	 */
	public short getType(){
		return this.type;
	}
	
	/**
	 * Sets the entity type
	 * 
	 * @param t the type of the entity INDIVDUAL, INSTITUTION, UNKNOWN
	 */
	
	public void setType(short t){
		this.type = t;
	}
	
	/**
	 *  Merges an entities attributes into this one
	 *  
	 *  A new object is created for each attribute and then added to this entity
	 *  where it's content does not already exist.
	 * 
	 * @param toMerge The entity who's attributes should be merged into this one
	 * @param score The score derived when this record and toMerge were compared
	 */

	public void mergeWith(NormalisedRecord toMerge, double score){
		for(NormalisedName n : toMerge.getNormalisedNames()){
			NormalisedName toAdd = new NormalisedName();
			toAdd.setSourceID(n.getSourceID());
			toAdd.setSourceName(n.getSourceName());
			toAdd.setSourceURL(n.getSourceURL());
			toAdd.setMatchScore(score);
			toAdd.setName(new String(n.getName()));
			toAdd.setNormalisedName(new String(n.getNormalisedName()));
			toAdd.setUsedFrom(new Date(n.getUsedFrom().getTime()));
			toAdd.setUsedUntil(new Date(n.getUsedUntil().getTime()));
		
		
			for(NormalisedNameComponent f : n.getFamilyNames()){
				NormalisedNameComponent fToAdd = new NormalisedNameComponent();
				fToAdd.setSourceID(n.getSourceID());
				fToAdd.setSourceName(n.getSourceName());
				fToAdd.setSourceURL(n.getSourceURL());
				fToAdd.setMatchScore(score);
				fToAdd.setChars(new String(f.chars));
				toAdd.getFamilyNames().add(fToAdd);
			}
			for(NormalisedNameComponent g : n.getGivenNames()){
				NormalisedNameComponent gToAdd = new NormalisedNameComponent();
				gToAdd.setSourceID(n.getSourceID());
				gToAdd.setSourceName(n.getSourceName());
				gToAdd.setSourceURL(n.getSourceURL());
				gToAdd.setMatchScore(score);
				gToAdd.setChars(new String(g.chars));
				toAdd.getGivenNames().add(gToAdd);
			}
			
			this.addNormalisedName(toAdd);
			
		}
		toMerge.getNormalisedNames().clear();
		for(NormalisedTitle t : toMerge.getNormalisedTitles()){
			NormalisedTitle toAdd = new NormalisedTitle();
			toAdd.setSourceID(t.getSourceID());
			toAdd.setSourceName(t.getSourceName());
			toAdd.setSourceURL(t.getSourceURL());
			toAdd.setMatchScore(score);
			toAdd.setTitle(new String(t.getTitle()));
			this.addNormalisedTitle(toAdd);
			
		}
		toMerge.getNormalisedTitles().clear();
		for(NormalisedFieldOfActivity f : toMerge.getNormalisedFieldsOfActivity()){
			NormalisedFieldOfActivity toAdd = new NormalisedFieldOfActivity();
			toAdd.setSourceID(f.getSourceID());
			toAdd.setSourceName(f.getSourceName());
			toAdd.setSourceURL(f.getSourceURL());
			toAdd.setMatchScore(score);
			toAdd.setFieldOfActivity(new String(f.getFieldOfActivity()));
			this.addNormalisedFieldOfActivity(toAdd);
			
		}
		toMerge.getNormalisedFieldsOfActivity().clear();
		for(NormalisedAffiliation a : toMerge.getNormalisedAffiliations()){
			NormalisedAffiliation toAdd = new NormalisedAffiliation();
			toAdd.setSourceID(a.getSourceID());
			toAdd.setSourceName(a.getSourceName());
			toAdd.setSourceURL(a.getSourceURL());
			toAdd.setMatchScore(score);
			toAdd.setAffiliationName(new String(a.getAffiliationName()));
			
			NormalisedRecord newAffiliation = new NormalisedRecord();
			for(NormalisedName aN : a.getAffiliation().getNormalisedNames()){
				NormalisedName newName = new NormalisedName();
				newName.setSourceID(aN.getSourceID());
				newName.setSourceName(aN.getSourceName());
				newName.setSourceURL(aN.getSourceURL());
				newName.setMatchScore(score);
				newName.setName(new String(aN.getName()));
				newName.setNormalisedName(new String(aN.getNormalisedName()));
				newName.setUsedFrom(new Date(aN.getUsedFrom().getTime()));
				newName.setUsedUntil(new Date(aN.getUsedUntil().getTime()));
				

				for(NormalisedNameComponent f : aN.getFamilyNames()){
					NormalisedNameComponent fToAdd = new NormalisedNameComponent();
					fToAdd.setSourceID(aN.getSourceID());
					fToAdd.setSourceName(aN.getSourceName());
					fToAdd.setSourceURL(aN.getSourceURL());
					fToAdd.setMatchScore(score);
					fToAdd.setChars(new String(f.chars));
					newName.getFamilyNames().add(fToAdd);
				}
				for(NormalisedNameComponent g : aN.getGivenNames()){
					NormalisedNameComponent gToAdd = new NormalisedNameComponent();
					gToAdd.setSourceID(aN.getSourceID());
					gToAdd.setSourceName(aN.getSourceName());
					gToAdd.setSourceURL(aN.getSourceURL());
					gToAdd.setMatchScore(score);
					gToAdd.setChars(new String(g.chars));
					newName.getGivenNames().add(gToAdd);
				}
				
				
				
				newAffiliation.addNormalisedName(newName);
			}
			toAdd.setAffiliation(newAffiliation);
			this.addNormalisedAffiliation(toAdd);
			
		}
		toMerge.getNormalisedAffiliations().clear();
		for(NormalisedResultPublication r : toMerge.getNormalisedResultPublications()){
			NormalisedResultPublication toAdd = new NormalisedResultPublication();
			toAdd.setSourceID(r.getSourceID());
			toAdd.setSourceName(r.getSourceName());
			toAdd.setSourceURL(r.getSourceURL());
			toAdd.setMatchScore(score);
			toAdd.setAbs(new String(r.getAbs()));
			toAdd.setDateOfPublication(new String(r.getDateOfPublication()));
			toAdd.setEdition(new String(r.getEdition()));
			toAdd.setIssue(new String(r.getIssue()));
			toAdd.setNumber(new String(r.getNumber()));
			toAdd.setSeries(new String(r.getSeries()));
			toAdd.setTitle(new String(r.getTitle()));
			toAdd.setVolume(new String(r.getVolume()));
			this.addNormalisedResultPublication(toAdd);
			
		}
		toMerge.getNormalisedResultPublications().clear();
		for(NormalisedIdentifier i : toMerge.getNormalisedIdentifiers()){
			NormalisedIdentifier toAdd = new NormalisedIdentifier();
			toAdd.setSourceID(i.getSourceID());
			toAdd.setSourceName(i.getSourceName());
			toAdd.setSourceURL(i.getSourceURL());
			toAdd.setMatchScore(score);
			toAdd.setIdentifier(i.getIdentifier());
			toAdd.setBasisFor(i.getBasisFor());
			this.addNormalisedIdentifier(toAdd);
		}
		toMerge.getNormalisedIdentifiers().clear();
	}

	public NormalisedRecord split(NormalisedRecord normalisedRecord){
		return null;
	}

	public boolean isIndividual(short type){
		return type == INDIVIDUAL ? true : false;
	}
	
	public boolean isInstitution(short type){
		return type == INSTITUTION ? true : false;
	}

	
}
