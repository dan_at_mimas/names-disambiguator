package uk.ac.mimas.names.disambiguator.util;

import java.util.ArrayList;
import java.util.HashSet;


import uk.ac.mimas.names.disambiguator.types.*;

import org.apache.log4j.Logger;


/**
 * Takes an input names string and attempts to pull out the following from it:
 * Salutation
 * Family names
 * Given names
 * Suffix
 * Previous names
 * 
 * @author danielneedham
 *
 */

public class PersonNameParser {
	private static final Logger logger = Logger.getLogger(PersonNameParser.class.getName());
	// General usage constants
	public static final int NO_COMMA = -1;
	public static final int LEFT = -2;
	public static final int RIGHT = -3;
	public static final int UNLIMITED = -4;
	// indicator constants
	public static final int STRING_START = -5;
	public static final int STRING_END = -6;
	public static final int TITLE = 2;
	public static final int SUFFIX = 3;
	public static final int COMMA = 4;
	// family name constants
	public static final int FAMILY_NAME = 10;
	public static final int COMPOUND_PREFIX = 11;
	// given name constants
	public static final int GIVEN_NAME = 20;
	public static final int INITIAL = 21;
	// other constants
	public static final int UNKNOWN = 66;
	
	// Number of passes to perform before finishing
	public static int MAX_PASSES = 5;
	
	ArrayList<NormalisedTitle> titles;												// derived salutations
	ArrayList<NormalisedNameComponent> familyNameComponents;						// derived family names
	ArrayList<NormalisedNameComponent> givenNameComponents;							// derived given names
	ArrayList<String> suffix;														// derived suffixes
	ArrayList<NormalisedName> previousNames; 										// derived previous names ; may require parsing itself
	
	
	// Various pre-defined name elements 
	private static final String[] invalidPhrases = {"et al"};
	private static final String regexTemporalIndicators = "nee|see|was|now";
	private static HashSet<String> commonFamilyNames;
	private static HashSet<String> commonGivenNames;
	private static HashSet<String> commonTitles;
	private static HashSet<String> commonSuffixes;
	private static HashSet<String> compoundPrefixes;
	
	// The elements making up the name
	public ArrayList<NameElement> nameElements;
	
	public String normalisedName = "";
	public int commaPos = NO_COMMA;
	public int familyNamesStart = 0;
	public int givenNamesStart = 0;
	public boolean containsUnknown;

	
	/**
	 * Name element definition
	 * A name is considered to be made up of one or more of these elements
	 * 
	 * @author danielneedham
	 *
	 */
	public class NameElement{
		public int position; 					// The position in the name string
		public String chars;					// The characters that make up this element
		public int type;						// The type of the element
	
	}
	
	/**
	 * 
	 * Constructor
	 * 
	 */
	public PersonNameParser(){
		
		familyNameComponents= new ArrayList<NormalisedNameComponent>();
		givenNameComponents = new ArrayList<NormalisedNameComponent>();
		titles = new ArrayList<NormalisedTitle> ();
		suffix = new ArrayList<String>();
		previousNames =  new ArrayList<NormalisedName>();
		commonFamilyNames = new HashSet<String>();
		/*
		 * A list of common family names can be added here
		 */
		//commonFamilyNames.add("smith");
		//commonFamilyNames.add("jones");
		//commonFamilyNames.add("hall");
		commonGivenNames = new HashSet<String>();
		/*
		 * A list of common given names can be added here
		 */
		//commonGivenNames.add("john");
		//commonGivenNames.add("jone");
		//commonGivenNames.add("ben");
		
		// Add common salutations
		commonTitles = new HashSet<String>();
		commonTitles.add("mr");
		commonTitles.add("mrs");
		commonTitles.add("dr");
		commonTitles.add("prof");
		commonTitles.add("professor");
		// Add common suffixes
		commonSuffixes = new HashSet<String>();
		commonSuffixes.add("I");
		commonSuffixes.add("II");
		commonSuffixes.add("III");
		commonSuffixes.add("IV");
		commonSuffixes.add("V");
		commonSuffixes.add("senior");
		commonSuffixes.add("junior");
		commonSuffixes.add("jr");
		commonSuffixes.add("sr");
		commonSuffixes.add("phd");
		commonSuffixes.add("msc");
		commonSuffixes.add("bsc");
		commonSuffixes.add("fmedsci");
		commonSuffixes.add("cbe");
		// Add common prefixes
		compoundPrefixes = new HashSet<String>();
		compoundPrefixes.add("da");
		compoundPrefixes.add("de");
		compoundPrefixes.add("del");
		compoundPrefixes.add("des");
		compoundPrefixes.add("dos");
		compoundPrefixes.add("di");
		compoundPrefixes.add("du");
		compoundPrefixes.add("von");
		compoundPrefixes.add("van");
		compoundPrefixes.add("—");
		nameElements = new ArrayList<NameElement>();

	}
	

	/**
	 * Makes a best guess at parsing the various name elements from a string.
	 * 
	 * Ideas of what a source name might look like:
	 * 
	 * John Smith
	 * Dr John Smith
	 * Smith, J.
	 * Smith, Mr J.
	 * Smith John J
	 * J J Smith
	 * Jone Smith (nee Hall)
	 * J Smith et al
	 * ...
	 * 
	 * @param nameString The source name string
	 */
	public void parse(String nameString){
		NormalisedNameComponent newComponent = null;
		containsUnknown = true;
		commaPos = NO_COMMA;
	
		try{
			// 1. Normalise the string
			this.normalisedName = nameString;																		
			for(String phrase : invalidPhrases) nameString = nameString.replaceAll("\\b"+phrase+"\\b", "").trim(); 	// 1a. Remove any pre-specified phrases - eg et al
			nameString = nameString.replaceAll("\\(.+?\\)","").trim();												// 1b. Deal with anything in brackets for now we'll just remove them
			nameString = nameString.replaceAll("(^,)|(,$)","").trim();												// 1c. Check if the name begins or ends with a comma if it does then remove it.
			if(nameString.contains(",")){																			// 1d. Names may take the form name,name or name , name. we'll prefer the latter
				nameString = fixCommas(nameString).trim();
			}
			if(nameString.contains("."))																			// 1e. Fix fullstops (add a space after any that don't have any)
				nameString = fixFullStops(nameString).trim();
			nameString = nameString.replaceAll("[^\\s\\w\\d,-\\.\\\\'\\p{L}\\p{M}]", "").trim();					// 1f. Remove any punctuation that we don't want to deal with replace with \s
			nameString = nameString.replaceAll("\\s{2,}", " ").trim();												// 1g. Replace any multi white space with single space
			this.normalisedName = nameString;
		
			// 2. Iterate through what's left and try and determine its type.
			String[] nameStringElements = nameString.split("\\s");													// 2a. split on spaces
			for(String nameElement : nameStringElements){															// 2b. Initialise the name elements , and fill some indicators
				NameElement n = new NameElement();
				n.chars = nameElement;
				n.position = nameElements.size();
				n.type = UNKNOWN;
				if(n.chars.equalsIgnoreCase(",")){
					n.type = COMMA;
					commaPos = n.position;
			
				}
				nameElements.add(n);
			}
				
			int numPasses = 0;				
			boolean containsKnown = false;																			
			while(numPasses < MAX_PASSES){																			// 2c. perform MAX_PASSES passes over elements trying to determine what they are
				for(NameElement n : nameElements){
					if(n.type != UNKNOWN)																				// We have already determined the type so we can ignore it
						continue; 
																														// Get the types of the surrounding elements (May be UNKNOWN)
					int previousElementType = getType(LEFT, n.position);
					int nextElementType = getType(RIGHT, n.position);
					int nearestKnownLeftType = nearestKnown(LEFT, n.position) == STRING_START ? STRING_START : nameElements.get(nearestKnown(LEFT, n.position)).type;
					int nearestKnownRightType = nearestKnown(RIGHT,n.position) == STRING_END? STRING_END : nameElements.get(nearestKnown(RIGHT,n.position)).type;
					boolean precedesComma = commaPos != NO_COMMA ? precedes(n.position, commaPos) : false;				// if there's a comma does this element precede it
					boolean proceedsComma = commaPos != NO_COMMA ? proceeds(n.position, commaPos) : false;				// if there's a comma does this element proceed it
					
					// Is this a one element given name?
					if(previousElementType == STRING_START && nextElementType == STRING_END){							// firstly check if this element is the only one this deals with G(+). One string names are considered to be given names
						n.type =  GIVEN_NAME;
						continue;
					}
					
					// Is this a title?
					
					if(isTitle(n)){																						
						if((previousElementType == STRING_START && (checkTypeRange(GIVEN_NAME, nextElementType) || checkTypeRange(FAMILY_NAME, nextElementType )|| nextElementType == UNKNOWN))){
							n.type = TITLE;
							continue; 																					// it's not at the beginning followed by a given name or family name : Mr needham || Mr Daniel Needham
						}
						if((previousElementType == COMMA && (checkTypeRange(GIVEN_NAME, nextElementType) || nextElementType == UNKNOWN))){
							n.type = TITLE;
							continue; 																					// it's not preceded by a comma and followed by a given name : Needham, Mr D. 
						}
						if((previousElementType == STRING_END && (checkTypeRange(GIVEN_NAME, previousElementType) || previousElementType == UNKNOWN))){
							n.type = TITLE;
							continue; 																					// it's not at the end preceeded by a given name
						}
					}
					
					// Is this a suffix?
				
					if(isSuffix(n)){
						if((nextElementType == STRING_END && (checkTypeRange(FAMILY_NAME, previousElementType) || previousElementType == SUFFIX || previousElementType == UNKNOWN))){
							n.type = SUFFIX;
							continue; 																					// not at the end of a string an preceeded by a family name type
						}
						if((previousElementType == STRING_START &&(checkTypeRange(FAMILY_NAME, nextElementType) || nextElementType == SUFFIX || nextElementType == UNKNOWN))){
							n.type = SUFFIX;
							continue; 																					// not at the beginning of a string followed by a family name or suffix
						}
						if(
							 (
								(checkTypeRange(FAMILY_NAME, previousElementType) || previousElementType == SUFFIX || (previousElementType == UNKNOWN && nextElementType != UNKNOWN)) 
								&& 
								(checkTypeRange(FAMILY_NAME, nextElementType) || nextElementType == SUFFIX || nextElementType == COMMA || (nextElementType == UNKNOWN && previousElementType != UNKNOWN))
							 )
						){
							n.type = SUFFIX;
							continue; 																					// not preceeded by a family name or suffix and proceeded by a family name or suffix or comma
						}
						
					}
					
					// Is it a compound prefix?
					if(isCompoundPrefix(n)){
						if((previousElementType == STRING_START && (checkTypeRange(FAMILY_NAME,nextElementType) || nextElementType == UNKNOWN))){
							n.type = COMPOUND_PREFIX;
							continue; 																					// it isn't at the start of the string proceeded by a family name 
						}
						if((nextElementType == STRING_END && (checkTypeRange(GIVEN_NAME, previousElementType) || previousElementType == UNKNOWN))){
							n.type = COMPOUND_PREFIX;
							continue; 																					// it isn't at the end of the string preceeded by a given name (may need to add family name to this
						}
						if((previousElementType == COMMA || (checkTypeRange(GIVEN_NAME, previousElementType)|| previousElementType == UNKNOWN )&&
								(checkTypeRange(FAMILY_NAME,nextElementType) || nextElementType == UNKNOWN))){
							n.type = COMPOUND_PREFIX;
							continue; 																					// not preceeded by a comma or a given name and proceeded by a family name.
						}
					}
				
					// Is it an initial?
					if(isInitial(n)){
						/*
						 * in instances where there is no comma
						 * we do our best to see what's around, but if there's
						 * no indication that it's not an initial
						 * we have to assume it is.
						 * 
						 */
	
						if(previousElementType == STRING_START || isInitial(nameElements.get(n.position -1))){				// initials generally stick together so lets try and see what's around first
							n.type = INITIAL;
							continue;
						}
						
						if(nextElementType == STRING_END || isInitial(nameElements.get(n.position +1))){
							n.type = INITIAL;
							continue;
						}
						
						if(n.chars.endsWith(".")){																			// if all else fails we'll consider an initial ending in a '.' as an initial in  any circumstance
							n.type = INITIAL;
							continue;
						}
					
					}

					if(n.type == UNKNOWN){ 																					// Should be still UNKNOWN at this point
						NameElement startElement = nameElements.get(0);
						NameElement endElement = nameElements.get(nameElements.size() -1);
						NameElement beforeComma= null;
						NameElement afterComma = null;
						if(commaPos != NO_COMMA){
							beforeComma = nameElements.get(commaPos -1);
							afterComma = nameElements.get(commaPos +1); 
						}
						
																															// The element is still UNKNOWN so we must use the known elements around it to try and determine it's type. These will be documented separately.
						if(isGivenName(n) && !isFamilyName(n)){																
							n.type = GIVEN_NAME;
							continue;
						}
						else if(isFamilyName(n) && ! isGivenName(n)){														
							n.type = FAMILY_NAME;
							continue;
						}
						
						if(nextElementType == SUFFIX)																		
						{
							n.type = FAMILY_NAME;
							continue;
						}
						
						if(previousElementType == COMPOUND_PREFIX || (previousElementType == STRING_START && endElement.type == COMPOUND_PREFIX))	
						{
							n.type = FAMILY_NAME;
							continue;
						}
						
	
						if(nextElementType == COMPOUND_PREFIX || (nextElementType == STRING_END && startElement.type == COMPOUND_PREFIX))
						{
							n.type = GIVEN_NAME;
							continue;
						}
						
						if(previousElementType == TITLE && (nextElementType != UNKNOWN && nextElementType != SUFFIX)){
							n.type = GIVEN_NAME;
							continue;
						}
						
					
						
						if(checkTypeRange(FAMILY_NAME, previousElementType) && (checkTypeRange(FAMILY_NAME, nextElementType) || nextElementType == COMMA)){
							n.type = FAMILY_NAME;
							continue;
						}
						
						if((checkTypeRange(GIVEN_NAME, previousElementType) || previousElementType == COMMA) &&checkTypeRange(GIVEN_NAME, nextElementType) ){
							n.type = GIVEN_NAME;
							continue;
						}
						
						
						if((checkTypeRange(FAMILY_NAME, nextElementType) || nextElementType == COMMA) &&checkTypeRange(FAMILY_NAME, previousElementType) ){
							n.type = FAMILY_NAME;
							continue;
						}
						if(checkTypeRange(GIVEN_NAME, previousElementType) && nextElementType == COMMA && checkTypeRange(GIVEN_NAME, endElement.type) ){
							n.type = FAMILY_NAME;
							continue;
						}
						
						if(																											// anything that is inbetween a given name or title and a compound prefix or family name is considered a given name a family name can only follow a compound prefix forward or in reverse or be joined by an hyphen 
								(checkTypeRange(GIVEN_NAME,previousElementType) && checkTypeRange(FAMILY_NAME,nextElementType))
								||
								(checkTypeRange(FAMILY_NAME,previousElementType) && checkTypeRange(GIVEN_NAME,nextElementType))
						){
							n.type = GIVEN_NAME;
							continue;
						}
						
						if(previousElementType == STRING_START){
							if(checkTypeRange(GIVEN_NAME, nextElementType)
									&& checkTypeRange(GIVEN_NAME, endElement.type)){
								n.type = FAMILY_NAME;
								continue;
							}
							if(checkTypeRange(FAMILY_NAME, nextElementType)
									&& checkTypeRange(FAMILY_NAME, endElement.type)){
								n.type = GIVEN_NAME;
								continue;
							}
							
							if(checkTypeRange(FAMILY_NAME, nextElementType)
									&& checkTypeRange(GIVEN_NAME, endElement.type)){
								n.type = FAMILY_NAME;
								continue;
							}
							if(checkTypeRange(GIVEN_NAME, nextElementType)
									&& checkTypeRange(FAMILY_NAME, endElement.type)){
								n.type = GIVEN_NAME;
								continue;
							}
							
							
							if(precedesComma && (nextElementType == COMMA || nextElementType == UNKNOWN)){
								if((afterComma.type == TITLE || checkTypeRange(GIVEN_NAME, afterComma.type))){
									n.type = FAMILY_NAME;
									continue;
								}
								if((afterComma.type == SUFFIX || checkTypeRange(FAMILY_NAME, afterComma.type))){
										n.type = GIVEN_NAME;
										continue;
								}
								
							}
							if(commaPos == NO_COMMA && nextElementType == UNKNOWN){
								if(endElement.type == SUFFIX || checkTypeRange(FAMILY_NAME, endElement.type)){
									n.type = GIVEN_NAME;
									continue;
								}
								if(endElement.type == TITLE || checkTypeRange(GIVEN_NAME, endElement.type)){
									n.type = FAMILY_NAME;
									continue;
								}
								
							}
						}
						
						if(nextElementType == STRING_END){
							if(checkTypeRange(GIVEN_NAME, previousElementType)
									&& checkTypeRange(GIVEN_NAME, startElement.type)){
								n.type = FAMILY_NAME;
								continue;
							}
							if(checkTypeRange(FAMILY_NAME, previousElementType)
									&& checkTypeRange(FAMILY_NAME, startElement.type)){
								n.type = GIVEN_NAME;
								continue;
							}
						
							if(commaPos == NO_COMMA && previousElementType == UNKNOWN){
								if(startElement.type == TITLE || checkTypeRange(GIVEN_NAME, startElement.type)){
									n.type = FAMILY_NAME;
									continue;
								}
								if(startElement.type == SUFFIX || checkTypeRange(FAMILY_NAME, startElement.type)){
									n.type = GIVEN_NAME;
									continue;
								}
								
							}
							
							
							/*
							 *  If we've got to this point and we still don't know what any of the elements
							 *  in the name are then we're just going to have to make a best guess.
							 *  
							 *  Names with no comma are assumed to have a family name at the end.
							 *  Otherwise they are assumed to have a given name at the end.
							 * 
							 */
							if(!containsKnown){
								
								if(commaPos == NO_COMMA){
									n.type = FAMILY_NAME;
									continue;
								}
								else{
									n.type = GIVEN_NAME;
									continue;
								}
							}
						}
					
						if(checkTypeRange(FAMILY_NAME,nearestKnownLeftType) && (checkTypeRange(FAMILY_NAME,nearestKnownRightType) || nearestKnownRightType == COMMA || nearestKnownRightType == SUFFIX)){
							n.type = FAMILY_NAME;
							continue;
						}
						
						if(checkTypeRange(GIVEN_NAME,nearestKnownRightType) && (checkTypeRange(GIVEN_NAME,nearestKnownLeftType) || nearestKnownLeftType == COMMA || nearestKnownLeftType == TITLE)){
							n.type = GIVEN_NAME;
							continue;
						}
						
						
						if(n.type != UNKNOWN && n.type != COMMA){
							containsKnown = true;
							
						}
					
					}
					
					
				}
				numPasses ++;
		}

			
		
			
		for(NameElement nameElement : nameElements){												// 2d. Add the various elements that were found
			if(nameElement.type == UNKNOWN){
				containsUnknown = true;
				continue;
			}
			else{
				containsUnknown = false;
			}
			if(checkTypeRange(GIVEN_NAME,nameElement.type)){
				    newComponent  = new NormalisedNameComponent();
					newComponent.setChars(nameElement.chars);
					givenNameComponents.add(newComponent);
					continue;
			}
			if(checkTypeRange(FAMILY_NAME,nameElement.type)){
			    newComponent  = new NormalisedNameComponent();
				newComponent.setChars(nameElement.chars);
				familyNameComponents.add(newComponent);
				continue;
			}
			if(nameElement.type == TITLE){
				NormalisedTitle newTitle = new NormalisedTitle();
				newTitle.setTitle(nameElement.chars);
				titles.add(newTitle);
			}
			if(nameElement.type == SUFFIX){
				
				suffix.add(nameElement.chars);
			}
			
		}	
		}
		catch(Exception e){
			logger.error("Error parsing: " + nameString);
		
		}
	}
	
	/**
	 * Specifies whether the passed element chars could be considered an initial
	 * @param n the name element to look at
	 * @return True if the chars of the name element look like an initial. False if not.
	 */
	
	public boolean isInitial(NameElement n){
		if((!Character.isDigit(n.chars.charAt(0)) && (n.chars.length() == 1 ||
			(n.chars.length() == 2 && n.chars.charAt(1) == '.'))))
			return true;
		return false;
	}
	
	/**
	 * Specifies whether the passed element chars could be considered a title
	 * @param n the name element to look at
	 * @return True if the chars of the element look like a title. False if not.
	 */
	public boolean isTitle(NameElement n){
		String test = n.chars.replaceAll("[^\\w]", "").toLowerCase().trim();
		return commonTitles.contains(test);		
	}
	/**
	 * Specifies whether the passed element chars look like a suffix
	 * @param n the name element to look at
	 * @return True if the chars of the element look like a suffix. False if not.
	 */
	public boolean isSuffix(NameElement n){
		String test = n.chars.replaceAll("[^\\w]", "").toLowerCase().trim();
		return commonSuffixes.contains(test);		
	}
	/**
	 * Specifies whether the passed element chars look like a family name
	 * @param n the name element to look at
	 * @return True if the chars of the element look like a family name. False if not.
	 */
	public boolean isFamilyName(NameElement n){
		
		if(		
				commonFamilyNames.contains(n.chars.toLowerCase()) ||
				n.chars.contains("'") ||
				isCamelCased(n.chars)
		){
				return true;
		}
	
		return false;
	}
	
	/**
	 * Specifies whether the passed element chars look like a given name
	 * @param n the name element to look at
	 * @return True if the chars of the element look like a given name. False if not.
	 */
	public boolean isGivenName(NameElement n){
		return commonGivenNames.contains(n.chars.toLowerCase());
	}
	

	/**
	 * Specifies whether the passed element chars look like a compound prefix
	 * @param n the name element to look at
	 * @return True if the chars of the element look like a compound prefix. False if not.
	 */
	public boolean isCompoundPrefix(NameElement n){
		String test = n.chars.toLowerCase().trim();
		return compoundPrefixes.contains(test);	
	}
	
	/*
	 * Checks whether string is camel cased
	 * Requires at least two upper case and two lower case
	 * chars to be classified as such
	 */
	/**
	 * Checks whether the passed string is camel cased.
	 * At least two upper case and two lower case characters must be present to 
	 * be classified as such.
	 * 
	 * @param chars The string to examine
	 * @return True if there are two or more upper and lower case characters. Otherwise false.
	 */
	public boolean isCamelCased(String chars){
		int numUpperCase, numLowerCase;
		numUpperCase = numLowerCase = 0;
		for(int i =0; i < chars.length(); i++){
			if(Character.isUpperCase(chars.charAt(i)))
				numUpperCase++;
			if(Character.isLowerCase(chars.charAt(i)))
				numLowerCase++;
			if(numUpperCase > 1 && numLowerCase > 1)
				return true;
		}
		return false;
	}
	
	/**
	 * Returns all the found name elements for the source name string
	 * 
	 * @return The name elements found for the source string.
	 */
	public ArrayList<NameElement> getNameElements(){
		return nameElements;
	}
	
	
	// Places spaces around the first comma it finds
	// erases any subsequent commas as they are deemed invalid
	
	/**
	 * Places a space around the first comma it finds, and erases and subsequent commas.
	 * 
	 * @param nameString The nameString to check
	 * @return A string with normalised commas.
	 */
	private String fixCommas(String nameString){
		String outString = "";
		boolean foundComma = false;
		for(int i =0; i < nameString.length() ;i++){
		
			if(nameString.charAt(i) == ',' && i != 0 && nameString.charAt(i - 1) != ' ' && foundComma == false)
				outString += ' ';
			outString += nameString.charAt(i);
			if(nameString.charAt(i) == ',' && i != (nameString.length() -1) && nameString.charAt(i + 1) != ' ' && foundComma == false)
				outString += ' ';
			if(nameString.charAt(i) == ',' )
				foundComma = true;
		}
		return outString;
	}
	
	/**
	 * Places a space after every full stop if one doesn't already exist. 
	 * Mainly helps to identify initials.
	 * 
	 * @param nameString The name string to check
	 * @return A name string with all initials padded by spaces.
	 */
	private String fixFullStops(String nameString){
		String outString = "";
		for(int i =0; i < nameString.length() ;i++){
			outString += nameString.charAt(i);
			if(nameString.charAt(i) == '.' && i != (nameString.length() -1) && nameString.charAt(i + 1) != ' ')
				outString += ' ';
		}
		return outString;
	}
	
	
	/**
	 * Checks whether the specified type is within a a particular range of element types
	 * 
	 * At the moment, a family name type or a given name type
	 * 
	 * @param type The type range to check against
	 * @param value The value to check
	 * @return True if within the range, otherwise false.
	 */
	public boolean checkTypeRange(int type, int value){
		if(type == FAMILY_NAME && (value >= 10 && value < 20))
			return true;
		if(type == GIVEN_NAME && (value >= 20 && value < 30))
			return true;
		return false;
	}
	
	/**
	 * Checks whether the specified element position is before another position
	 * @param elementPosition this elements position
	 * @param checkPosition   another elements position
	 * @return True if elementPosition is before checkPosition, otherwise false
	 */
	private boolean precedes(int elementPosition, int checkPosition){
		if(elementPosition < checkPosition)
			return true;
		return false;
	}
	
	/**
	 * Checks whether the specified element position is after another position
	 * @param elementPosition this elements position
	 * @param checkPosition   another elements position
	 * @return True if elementPosition is after checkPosition, otherwise false
	 */
	private boolean proceeds(int elementPosition, int checkPosition){
		if(elementPosition > checkPosition)
			return true;
		return false;
	}
	
	/**
	 * Get the type of an element in a particular direction from the current position
	 * 
	 * @param direction The direction to look
	 * @param current_position The position to look from
	 * @return The type of the element at the specified location
	 */
	private int getType(int direction, int current_position){
		int r = (direction == LEFT) ? STRING_START : STRING_END;
		int checkPosition = current_position;
		checkPosition = (direction == LEFT) ? checkPosition - 1 : checkPosition + 1;
		r = checkPosition >= 0 && checkPosition < nameElements.size() ? nameElements.get(checkPosition).type : r;
		return r;
	}
	
	/**
	 * Look in the specified direction from the current position for the nearest known element type
	 * I.E not UNKNOWN
	 * @param direction Direction to look in
	 * @param current_position The position to look from.
	 * @return The position of the nearest known element
	 */
	private int nearestKnown(int direction, int current_position){
		//System.out.println("Check " + direction + " from " + current_position);
		int checkPosition = current_position;
		while(true){
			checkPosition = (direction == LEFT) ? checkPosition - 1 : checkPosition + 1;
			//System.out.println("check position: " + checkPosition);
			if(checkPosition < 0)
				return STRING_START;
			if(checkPosition == nameElements.size())
				return STRING_END;
			if(nameElements.get(checkPosition).type != UNKNOWN){
				//System.out.println("TYPE: " + nameElements.get(checkPosition).type);
				return checkPosition;
			}
		}
		//return 0;
	}

		
	/**
	 * Clear the results from a previous attempt at name parsing.
	 */
	
	public void clear(){
		titles = new ArrayList<NormalisedTitle>();
		familyNameComponents = new ArrayList<NormalisedNameComponent>();
		givenNameComponents = new ArrayList<NormalisedNameComponent>();
		suffix = new ArrayList<String>();
		previousNames =  new ArrayList<NormalisedName>(); // may require parsing itself
		normalisedName = "";
		commaPos = NO_COMMA;
		familyNamesStart = 0;
		givenNamesStart = 0;
		nameElements.clear();
		
	}

	/**
	 * Get any found titles 
	 * @return The titles from the source name. Empty if none found.
	 */
	public ArrayList<NormalisedTitle> getTitles() {
		return titles;
	}

	/**
	 * Set the titles from the original name
	 * @param titles The normalised titles for a name
	 */
	public void setTitles(ArrayList<NormalisedTitle> titles) {
		this.titles = titles;
	}

	/**
	 * Get any found family name components
	 * @return The family name components from the source name. Empty if none found.
	 */
	public ArrayList<NormalisedNameComponent> getFamilyNameComponents() {
		return familyNameComponents;
	}

	/**
	 * Set the family name components from the original name
	 * @param familyNameComponents The normalised family name components for a name
	 */
	public void setFamilyNameComponents(
			ArrayList<NormalisedNameComponent> familyNameComponents) {
		this.familyNameComponents = familyNameComponents;
	}

	/**
	 * Get any found given name components 
	 * @return The given name components from the source name. Empty if none found.
	 */
	public ArrayList<NormalisedNameComponent> getGivenNameComponents() {
		return givenNameComponents;
	}

	/**
	 * Set the given name components from the original name
	 * @param givenNameComponents The normalised given name components for a name
	 */
	public void setGivenNameComponents(ArrayList<NormalisedNameComponent> givenNameComponents) {
		this.givenNameComponents = givenNameComponents;
	}

	/**
	 * Get any found suffixes
	 * @return The suffixes from the source name. Empty if none found.
	 */
	public ArrayList<String> getSuffix() {
		return suffix;
	}
	/**
	 * Set the suffixes from the original name
	 * @param titles The suffixes for a name
	 */
	public void setSuffix(ArrayList<String> suffix) {
		this.suffix = suffix;
	}
}

