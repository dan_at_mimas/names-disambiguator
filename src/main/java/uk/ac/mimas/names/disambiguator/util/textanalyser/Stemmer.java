package uk.ac.mimas.names.disambiguator.util.textanalyser;

/**
 * String stemming class, adapted from the porter stemmer algorithm (Java): 
 * http://tartarus.org/~martin/PorterStemmer/
 * American/English language only at the moment.
 *  
 * @author danielneedham
 *
 */
public class Stemmer {
	
	private String stemmed = "";
	public char[] w;
	private int iniLength, curPosition, workingPosition, endPosition;
	
	/**
	 * Constructor
	 * 
	 */
	public Stemmer(){
		this.w = null;
		this.iniLength = 0;
		this.curPosition = 0;
		this.workingPosition = 0;
		this.endPosition = 0;
	}
	
	/**
	 * Attempt to stem an input string
	 * @param s The string to stem
	 * @return The stemmed string
	 */
	public String stem(String s){
		try{
			
			
			this.w = s.toCharArray();
			this.iniLength = w.length;
			this.curPosition = this.iniLength -1;
			if(w != null && this.curPosition > 1){
				this.stepOne();
				this.stepTwo();
				this.stepThree();
				this.stepFour();
				this.stepFive();
				this.stepSix();
			}
		
			this.endPosition = this.curPosition + 1;
			
			
		}
		catch(Exception e){
			
			
		}
		return this.stemmed;
	}
	
	/**
	 * Clear the results from a previous stem attempt
	 */
	public void clear(){
		stemmed = "";
		w = null;
		this.iniLength = 0;
		this.curPosition = 0;
		this.workingPosition = 0;
		this.endPosition = 0;
	}
	
	/**
	 * Step One
	 * remove plurals and -ed or -ing
	 */
	private void stepOne(){
		if(this.w[this.curPosition] == 's'){
			if(this.endsWith("sses"))
				this.curPosition -= 2;
			else if(this.endsWith("ies"))
				this.setTo("i");
			else if(this.w[this.curPosition -1] != 's')
				this.curPosition --;
			
		}
		if(this.endsWith("eed")){
			if(this.measure() > 0){
				this.curPosition --;
			}
		}
		if((this.endsWith("ed")||this.endsWith("ing")) && this.vowelInStem()){
			this.curPosition = this.workingPosition;
			if(this.endsWith("at"))
				this.setTo("ate");
			else if(this.endsWith("bl"))
				this.setTo("ble");
			else if(this.endsWith("iz"))
				this.setTo("ize");
			else if(this.doubleConsonant(this.curPosition)){
				this.curPosition --;
				if(this.w[this.curPosition] == 'l' || this.w[this.curPosition] == 's' || this.w[this.curPosition] == 'z'){
					this.curPosition ++;
				}
			}
			else if(this.measure() == 1 && this.isConsonantVowelConsonant(this.curPosition))
				this.setTo("e");
		}
	}
	

	/**
	 * Step Two
	 * Replaces teminal y with i if there is a vowel in the stem
	 */
	private void stepTwo(){
		if(this.endsWith("y") && this.vowelInStem())
			this.w[this.curPosition] = 'i';
	}
	
	/**
	 * Step Three
	 * 
	 *  reduces any double suffices ('ization' -> 'ize' ,  'ation') to  
	 *  single suffix where the stem has a measure of greater than 0
	 */
	private void stepThree(){
		if(this.curPosition == 0)
			return;
		
		// check second to last character
		switch(this.w[this.curPosition-1]){
			case 'a':
				if(this.endsWith("ational")){
					if(this.measure() >0)
						this.setTo("ate");
					break;	
				}
				if(this.endsWith("tional")){
					if(this.measure() >0)
						this.setTo("tion");
					break;	
				}
			break;
			case 'c':
				if(this.endsWith("enci")){
					if(this.measure() >0)
						this.setTo("ence");
					break;	
				}
				if(this.endsWith("anci")){
					if(this.measure() >0)
						this.setTo("ance");
					break;	
				}
			break;
			case 'e': 
				if(this.endsWith("izer")){
					if(this.measure() >0)
						this.setTo("ize");
					break;	
				}
			break;
			case 'l':
				if(this.endsWith("bli")){
					if(this.measure() >0)
						this.setTo("ble");
					break;	
				}
				if(this.endsWith("alli")){
					if(this.measure() >0)
						this.setTo("al");
					break;	
				}
				if(this.endsWith("entli")){
					if(this.measure() >0)
						this.setTo("ent");
					break;	
				}
				if(this.endsWith("eli")){
					if(this.measure() >0)
						this.setTo("e");
					break;	
				}
				if(this.endsWith("ousli")){
					if(this.measure() >0)
						this.setTo("ous");
					break;	
				}
			break;
			case 'o':
				if(this.endsWith("ization")){
					if(this.measure() >0)
						this.setTo("ize");
					break;	
				}
				if(this.endsWith("ation")){
					if(this.measure() >0)
						this.setTo("ate");
					break;	
				}
				if(this.endsWith("ator")){
					if(this.measure() >0)
						this.setTo("ate");
					break;	
				}
			break;
			case 's':
				if(this.endsWith("alism")){
					if(this.measure() >0)
						this.setTo("al");
					break;	
				}
				if(this.endsWith("iveness")){
					if(this.measure() >0)
						this.setTo("ive");
					break;	
				}
				if(this.endsWith("fulness")){
					if(this.measure() >0)
						this.setTo("ful");
					break;	
				}
				if(this.endsWith("ousness")){
					if(this.measure() >0)
						this.setTo("ous");
					break;	
				}
			break;
			case 't':
				if(this.endsWith("aliti")){
					if(this.measure() >0)
						this.setTo("al");
					break;	
				}
				if(this.endsWith("iviti")){
					if(this.measure() >0)
						this.setTo("ive");
					break;	
				}
				if(this.endsWith("biliti")){
					if(this.measure() >0)
						this.setTo("ble");
					break;	
				}
			break;
			case 'g':
				if(this.endsWith("logi")){
					if(this.measure() >0)
						this.setTo("log");
					break;	
				}
			break;
			
		}
	}
	

	/**
	 * Step Four
	 * 
	 * stems words with suffices containing 'ic' or ending in -ful, -ness
	 */
	private void stepFour(){
		switch(this.w[this.curPosition]){
			case 'e':
					if(this.endsWith("icate")){
						if(this.measure() > 0)
							this.setTo("ic");
						break;
					}
					if(this.endsWith("ative")){
						if(this.measure() > 0)
							this.setTo("");
						break;
					}
					if(this.endsWith("alize")){
						if(this.measure() > 0)
							this.setTo("al");
						break;
					}
			break;
			case 'i':
				if(this.endsWith("iciti")){
					if(this.measure() > 0)
						this.setTo("ic");
					break;
				}
			break;
			case 'l':
				if(this.endsWith("ical")){
					if(this.measure() > 0)
						this.setTo("ic");
					break;
				}
				if(this.endsWith("ful")){
					if(this.measure() > 0)
						this.setTo("");
					break;
				}
			break;
			case 's':
				if(this.endsWith("ness")){
					if(this.measure() > 0)
						this.setTo("");
					break;
				}
			break;
		}
	}
	
	/**
	 * Step Five
	 * takes off -ant, -ence etc., in context <c>vcvc<v>
	 */
	
	private void stepFive(){
		if(this.curPosition == 0)
			return;
		switch(this.w[this.curPosition -1]){
		case 'a': 
			if(this.endsWith("al"))
				break;
		return;
		case 'c':
			if(this.endsWith("ance"))
				break;
			if(this.endsWith("ence"))
				break;
		return;
		case 'e':
			if(this.endsWith("er"))
				break;
		return;
		case 'i':
			if(this.endsWith("ic"))
				break;
		return;
		case 'l':
			if(this.endsWith("able"))
				break;
			if(this.endsWith("ible"))
				break;
		return;
		case 'n':
			if(this.endsWith("ant"))
				break;
			if(this.endsWith("ement"))
				break;
			if(this.endsWith("ment"))
				break;
			if(this.endsWith("ent"))
				break;
		return;
		case 'o':
			if(this.endsWith("ion") && this.workingPosition >=0 && (this.w[this.workingPosition] == 's' || this.w[this.workingPosition] == 't'))
				break;
			if(this.endsWith("ou"))
				break;
		return;
		case 's':
			if(this.endsWith("ism"))
				break;
		return;
		case 't':
			if(this.endsWith("ate"))
				break;
			if(this.endsWith("iti"))
				break;
		return;
		case 'u':
			if(this.endsWith("ous"))
				break;
		return;
		case 'v':
			if(this.endsWith("ive"))
				break;
		return;
		case 'z':
			if(this.endsWith("ize"))
				break;
		return;
		default:
		return;
		}
		if(this.measure() > 1)
			this.curPosition = this.workingPosition;
	}
	

	/**
	 * Step Six
	 * remove any terminating e if measure is greater than 1
	 */
	private void stepSix(){
		this.workingPosition = this.curPosition;
		if(this.w[this.curPosition] == 'e'){
			if(this.measure() > 1 || this.measure() == 1 && !this.isConsonantVowelConsonant(this.curPosition -1))
				this.curPosition --;
			
		}
		
		if(this.w[this.curPosition] == 'l' && this.doubleConsonant(this.curPosition) && this.measure() >1)
			this.curPosition --;
	}
	
	
	
	
	/**
	 * Determines whether the current position and the two preceding elements
	 * form consonant, vowel, consonant, except where second consonant is w, x or y
	 * @param i Position in String
	 * @return True if forms CVC, false otherwise.
	 */
	private boolean isConsonantVowelConsonant(int i){
		if(i < 2 || !this.isConsonant(i)|| this.isConsonant(i-1)|| !this.isConsonant(i-2)){
			return false;
		}
		else{ 
			if(this.w[i] == 'w' || this.w[i] == 'x' || this.w[i] == 'y'){
				return false;
			}
		}
		return true;
	}
	

	/**
	 * Determines whether the current position and the preceding element form a double consonant.
	 * @param i Position
	 * @return True if double consonant, false otherwise.
	 */
	private boolean doubleConsonant(int i){
		if(i <1) 
			return false;
		if(this.w[i] != this.w[i -1]) 
			return false;
		return this.isConsonant(i);
	}
	

	/**
	 * Measures the number of consonant sequences from the beginning of the string
	 * to the current working position in the string.
	 * 
	 * @return Number of consonant sequences.
	 */
	private int measure(){
		int n =0;
		int i =0;
		
		while(true)
		{
			if( i > this.workingPosition) {
				return n;
			}
			if(!this.isConsonant(i))
				break;
			i++;
		}
		i++;
		while(true){
			while(true)
			{
				if( i > this.workingPosition) {
					return n;
				}
				if(this.isConsonant(i))	break;
				i++;
				
			}
			i++;
			n++;
			while(true)
			{
				if( i > this.workingPosition) {
					return n;
				}
				if(!this.isConsonant(i))
					break;
				i++;
			}
			i++;
		}
	}
	
	/**
	 * Does the string from 0 to the working position contain a vowel?
	 * @return True if contains a vowel, false otherwise
	 */
	private boolean vowelInStem(){
		for(int i =0; i <= this.workingPosition; i++)
			if(!this.isConsonant(i))
				return true;
		return false;
	}
	
	/**
	 * Is the passed char a consonant?
	 * @param i Char
	 * @return True if consonant, false otherwise
	 */
	private boolean isConsonant(int i){
		switch(this.w[i]){
			case 'a': case 'e' : case 'i' : case 'o': case 'u': return false;
			case 'y': return (i ==0) ? true : !this.isConsonant(i-1);
			default:return  true;
		}
	}
	

	/**
	 * Sets the characters in the working string at the working position to those in the passed string.
	 * @param s The characters to replace with at the workind position.
	 */
	private void setTo(String s){
		int offset = this.workingPosition + 1;
		for(int i =0; i < s.length(); i++)
			this.w[offset + i] = s.charAt(i);
		this.curPosition = this.workingPosition + s.length();
	}
	
	/**
	 * Does the sub string from 0 to current position end with the specified string
	 * @param s String to match
	 * @return True if the sub string ends with the specified string, false otherwise.
	 */
	private boolean endsWith(String s){
		int offset = this.curPosition - s.length() + 1;
		if(offset < 0)
			return false;	
		for(int i = 0 ; i < s.length(); i++){
			if(this.w[offset + i] != s.charAt(i))
				return false;
		}
		this.workingPosition = this.curPosition -  s.length();
		return true;
	}
	
	/**
	 * Get the final stemmed word
	 * @return The stemmed version of the source string
	 */
	public String getStemmedWord(){
		return new String(this.w, 0 ,this.endPosition);
	}
	
	/**
	 * Returns a string containing only valid characters from the input string
	 * @param s Source String
	 * @return The valid characters from the source string.
	 */
	public static String getValidCharacters(String s){
		StringBuffer temp = new StringBuffer();
		String r = null;
		try{
			for(int i = 0; i < s.length(); i++){
				if(Character.isLetter(s.charAt(i)))
					temp.append(s.charAt(i));
				else
					temp.append(" ");
			}
			r = temp.toString();
			
		}
		catch(Exception e){
			System.out.println("Error validating string" + e);
		}
		return r;
	}
}
