package uk.ac.mimas.names.disambiguator.types;
import java.util.ArrayList;
import uk.ac.mimas.names.disambiguator.NamesDisambiguator;
import java.util.Map;
import java.util.HashMap;
/**
 * 
 * Normalised affiliation
 * 
 * @author Dan Needham <daniel.needham@manchester.ac.uk>
 * 
 */

public class NormalisedAffiliation  extends NormalisedAttribute{
	
	public String affiliationName;
	public int affiliationNamesID;
	public NormalisedRecord affiliation;
	
	/** 
	 * Class constructor.
	 */
	
	public NormalisedAffiliation(){
		super();
		affiliationName = "";
		affiliationNamesID = -1;
		affiliation = new NormalisedRecord();
	}
	
	/** 
	 * Class constructor.
	 */
	
	public NormalisedAffiliation(String name){
		super();
		this.affiliationName = name;
		this.affiliationNamesID = -1;
		affiliation = new NormalisedRecord();
		
	}
	
	/** 
	 * Class constructor.
	 */
	
	public NormalisedAffiliation(String name, int id){
		super();
		this.affiliationName = name;
		this.affiliationNamesID = id;
		affiliation = new NormalisedRecord();
		
	}
	
	/**
	 * Gets the affiliation name
	 *
	 * @returns The name of the affiliation
	 */
	
	public String getAffiliationName() {
		return affiliationName;
	}
	
	/**
	 * Sets the affiliation name
	 *
	 * @param affiliationName The name of the affiliation
	 */
	
	public void setAffiliationName(String affiliationName) {
		this.affiliationName = affiliationName;
	}
	
	/**
	 * Gets the normalised record for the affiliation
	 * @return The affiliation normalised record
	 */
	public NormalisedRecord getAffiliation(){
		return this.affiliation;
	}
	/**
	 * Sets the normalised record for this affiliation
	 * @param r the normalised record to set.
	 */
	public void setAffiliation(NormalisedRecord r){
		this.affiliation = r;
	}
	
	
	
	/**
	 * @return the affiliationNamesID
	 */
	public int getAffiliationNamesID() {
		return affiliationNamesID;
	}

	/**
	 * @param affiliationNamesID the affiliationNamesID to set
	 */
	public void setAffiliationNamesID(int affiliationNamesID) {
		this.affiliationNamesID = affiliationNamesID;
	}

	/**
	 * Compare this affiliation against another one and derive a match score
	 * 
	 *
	 * @param comparisonRecord The normalised affiliation to compare with
	 */
	
	public double match(NormalisedAffiliation comparisonRecord){
		Map<String, Integer> weightings = new HashMap<String,Integer>();
		weightings.put(NamesDisambiguator.NAME_WEIGHT_KEY, 1); 						// The maximum weight the avg name match can have is 100%
		weightings.put(NamesDisambiguator.IDENTIFIER_WEIGHT_KEY, 100);				// The maxmum weight an identifier match can have is 100%
		weightings.put(NamesDisambiguator.TITLE_WEIGHT_KEY, 0);
		weightings.put(NamesDisambiguator.FIELD_OF_ACTIVITY_WEIGHT_KEY, 0);
		weightings.put(NamesDisambiguator.AFFILIATION_WEIGHT_KEY,0);
		weightings.put(NamesDisambiguator.RESULT_PUBLICATION_TITLE_WEIGHT_KEY, 0);
		weightings.put(NamesDisambiguator.RESULT_PUBLICATION_WORDMAP_WEIGHT_KEY, 0);
		NamesDisambiguator n = new NamesDisambiguator(weightings);

		return n.match(this.getAffiliation(), comparisonRecord.getAffiliation());
	}

}
