package uk.ac.mimas.names.disambiguator.types;

/**
 * Normalised Name Component
 * 
 * NormalisedNames contain a number of NormalisedNameComponents
 * 
 * @author danielneedham
 *
 */
public class NormalisedNameComponent  extends NormalisedAttribute{
		String chars;
		/**
		 * Constructor
		 */
		public NormalisedNameComponent(){
			super();
			chars = "";
		}
		/**
		 * Gets the characters for this component
		 * 
		 * @return The characters for this component
		 */
		public String getChars() {
			return chars;
		}
		
		/**
		 * Sets the characters for this component
		 * 
		 * @param chars The characters for this component
		 */
		public void setChars(String chars) {
			this.chars = chars;
		}
		
}
