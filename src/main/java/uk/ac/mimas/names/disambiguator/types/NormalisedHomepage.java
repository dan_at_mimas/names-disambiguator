package uk.ac.mimas.names.disambiguator.types;

/**
 * Normalised Record
 * 
 * @author danielneedham
 *
 */
public class NormalisedHomepage extends NormalisedAttribute{
			public String URL;
			
			/**
			 * Constructor
			 */
			public NormalisedHomepage(){
				super();
				URL = "";
			}

			/**
			 * Gets the url of the homepage
			 * @return The URL of the homepage
			 */
			public String getURL() {
				return URL;
			}
			
			/**
			 * Sets the URL of the homepage
			 * @param uRL The URL to set
			 */
			public void setURL(String uRL) {
				URL = uRL;
			}
			
			
}
