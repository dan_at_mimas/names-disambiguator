package uk.ac.mimas.names.disambiguator.util.textanalyser;

import java.util.HashMap;

import java.util.Map;

/**
 * 
 * Used to analyse the elements of a string
 * 
 * @author danielneedham
 *
 */
public class TextAnalyser {
	
		private String originalString;										// The original string
		private String normalisedString;									// The normalised string
		private int minimumStringLength;									// The minimum length that a sub string is to be considered a word
		private HashMap<String, Integer> wordMap; 							// Word / Frequency	
	
		/**
		 * Constructor
		 * @param minimum length that a sub string is considered a word.
		 */
		
		public TextAnalyser(int m){
			originalString = "";
			normalisedString = "";
			minimumStringLength = m;
			wordMap = new HashMap<String, Integer>();
		}
		
		/**
		 * Process an input string.
		 * 
		 * Essentially this involves normalising the input string,
		 * splitting it into sub strings and then stemming each valid sub string.
		 * 
		 * Once this is done a hashmap is filled containing all the valid words and the number of times they
		 * appeared
		 * 
		 * @param s The source string
		 */
		public void processString(String s){
			String temp;
			this.originalString = s;
			temp = this.originalString;
			temp = temp.toLowerCase();
			String[] sp = temp.split("");
			temp = "";
			Stemmer stemmer = new Stemmer();
			
			int lastPos;
			lastPos = 0;
			String testWord = new String();
			// cycle through lc'd string
			for(int i =0; i < sp.length; i++){
				// check for valid character
				sp[i] = Stemmer.getValidCharacters(sp[i]);
				if(sp[i].equalsIgnoreCase(" ") || i == (sp.length - 1)){
					for(int j = lastPos; j <= i; j++)
						testWord += sp[j];
					lastPos = i;
					testWord = testWord.trim();
					if(testWord.length() >= this.minimumStringLength){
						stemmer.stem(testWord.toString().trim());
						temp += stemmer.getStemmedWord() + " ";
					}
					testWord = "";
					stemmer.clear();
					
				}
			}
			
			stemmer.clear();
			
			this.normalisedString = temp;
			this.generateHashmap();
			
			
			
		}
		
		/**
		 * 
		 * Generate a hash map from the normalised string.
		 * 
		 * The hashmap contains each valid word that appeared in the normalised string against the number of times it appeared.
		 * 
		 * 
		 */
		private void generateHashmap(){
			String[] temp = this.normalisedString.split(" ");
			for(int i = 0 ; i < temp.length ; i ++){
				if(this.wordMap.containsKey(temp[i]))
					this.wordMap.put(temp[i], this.wordMap.get(temp[i])+1);
				else
					this.wordMap.put(temp[i], 1);
			}
		}
		
		/**
		 * Get the original string that was used to derive the hashmap
		 * @return The original string used to derive the hashmap
		 */
		public String getOriginalString(){
			return this.originalString;
		}
		/**
		 * Get the normalised string derived from the original string
		 * @return The normalised String derived from the original string
		 */
		public String getNormalisedString(){
			return this.normalisedString;
		}
		
		/**
		 * Set the minimum length at which a string is considered to be a word.
		 * @param m The minimum length
		 */
		public void setMinimumStringLength(int m){
			this.minimumStringLength = m;
		}
		
		/**
		 * Get the minimum length at which a string is considered a word.
		 * @return The minimum length.
		 */
		public int getMinimumStringLength(){
			return this.minimumStringLength;
			
		}
		
		/**
		 * Get the hash map generated from the input string.
		 * @return The hash map generated from the input string.
		 */
		public HashMap<String, Integer> getWordMap(){
			return this.wordMap;
		}
		
		/**
		 *  Compare the likeness of two word maps (hashmap of stemmed words and their frequency within a string of text) .
		 * 
		 * @param a A wordmap
		 * @param b Another wordmap
		 * @return The number of matching words between the two word maps
		 */
		public static int compareWordMaps(HashMap<String, Integer> a, HashMap<String, Integer> b){
			int match = 0;
			HashMap<String, Integer> largest = null;
			HashMap<String, Integer> smallest = null;
			if(a.size() > b.size()){
				largest = a;
				smallest = b;
			}
			else{
				largest = b;
				smallest = a;
			}
			
			for (Map.Entry<String, Integer> entry : largest.entrySet()) {
			    String word = entry.getKey();
			    //Integer frequency = entry.getValue(); 											// TODO Frequency is not used at the moment
			    if(smallest.containsKey(word))
			    	match ++;
			}
			
				
			return match;
		}
				
}
